package net.shadewind.racetrack.server;

import net.shadewind.racetrack.RacetrackException;

/**
 * Thrown to indicate that the client has done something in the game which is
 * not allowed.
 * 
 * @author Emil Eriksson
 */
public class GameException extends RacetrackException {
	private static final long serialVersionUID = 1L;

	public GameException()
	{
		super();
	}

	public GameException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public GameException(String arg0)
	{
		super(arg0);
	}

	public GameException(Throwable arg0)
	{
		super(arg0);
	}

}
