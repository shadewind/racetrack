package net.shadewind.racetrack.server;

/**
 * Main class for the server.
 * 
 * @author Emil Eriksson
 */
public class Main {
	/**
	 * Main entry point for the server application.
	 */
	public static void main(String[] args)
	{
		Server server = new Server();
		server.run();
	}
}
