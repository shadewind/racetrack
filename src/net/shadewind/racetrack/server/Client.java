package net.shadewind.racetrack.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.shadewind.racetrack.CommException;
import net.shadewind.racetrack.Command;
import net.shadewind.racetrack.RaceMap;
import net.shadewind.racetrack.RacetrackException;
import net.shadewind.racetrack.Score;
import net.shadewind.racetrack.Vector2i;

/**
 * Represents the connection to the client.
 * 
 * @author Emil Eriksson
 */
public class Client implements Runnable {
	private int id;
	private String name;

	private final Server server;
	private final Socket socket;
	private final PrintWriter out;
	private final BufferedReader in;
	private Game game;

	private static int nextId = 1;
	private static final Logger logger = Logger
			.getLogger("net.shadewind.racetrack.server.Client");

	/**
	 * Constructs a new client.
	 * 
	 * @param server the server object to associate the client with
	 * @param socket the socket through which the the client object will
	 *            communicate with the actual client
	 * 
	 * @throws IOException if any IOException is throw during the initialization
	 */
	public Client(Server server, Socket socket) throws IOException
	{
		synchronized (Client.class) {
			id = nextId++;
		}

		this.server = server;
		this.socket = socket;
		this.out = new PrintWriter(socket.getOutputStream());
		this.in = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
	}

	/**
	 * Returns the name of this client if known.
	 * 
	 * @return the client name if know, otherwise <code>null</code>
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Returns the ID of the client.
	 * 
	 * @return The ID.
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * Notifies the client that there is a player in the game which may or may
	 * not have existed previously.
	 * 
	 * @param id the player ID
	 * @param name the visible name of the player
	 */
	public void newPlayer(int id, String name)
	{
		Command command = new Command("player");
		command.append(id);
		command.append(name);
		sendCommand(command);
	}

	/**
	 * Notifies the client that there is a map available for the game master to choose.
	 * 
	 * @param id The ID of the map.
	 * @param name The name of the map.
	 */
	public void mapAvailable(int id, String name)
	{
		Command command = new Command("availableMap");
		command.append(id);
		command.append(name);
		sendCommand(command);
	}

	/**
	 * Notifies the client that the game is lobby mode.
	 * 
	 * @param gameMasterId  Notifies the client about which player is the game master.
	 */
	public void lobby(int gameMasterId)
	{
		sendCommand(new Command("lobby").append(gameMasterId));
	}
	
	/**
	 * Notifies the client that the game has started.
	 * 
	 * @param map                The map that was chosen.
	 * @param collisionsEnabled  Whether collisions are enabled.
	 */
	public void gameStarted(RaceMap map, boolean collisionsEnabled)
	{
		Command command = new Command("gameStarted");
		command.append(map.toNetworkString());
		command.append(collisionsEnabled);
		sendCommand(command);
	}
	
	/**
	 * Notifies the client that the specified players car has been reset to the
	 * specified position.
	 * 
	 * @param playerId  The ID of the player.
	 * @param pos       The position of the car.
	 */
	public void carReset(int playerId, Vector2i pos)
	{
		Command command = new Command("carReset");
		command.append(playerId);
		command.append(pos.x());
		command.append(pos.y());
		sendCommand(command);
	}
	
	/**
	 * Notifies the client that it's the specified players turn.
	 * 
	 * @param playerId  The ID of the player.
	 * @param timeout   The turn timeout.
	 */
	public void playerTurn(int playerId, int timeout)
	{
		Command command = new Command("playerTurn");
		command.append(playerId);
		command.append(timeout);
		sendCommand(command);
	}
	
	/**
	 * Notifies the client that the specified players car has been driven.
	 * 
	 * @param playerId  The players ID.
	 * @param position  The new position.
	 * @param velocity  The new velocity.
	 */
	public void carDriven(int playerId, Vector2i position, Vector2i velocity)
	{
		Command command = new Command("carDriven");
		command.append(playerId);
		command.append(position.x());
		command.append(position.y());
		command.append(velocity.x());
		command.append(velocity.y());
		sendCommand(command);
	}
	
	/**
	 * Notifies the client that the specified players car crashed.
	 * 
	 * @param playerId  The player ID.
	 */
	public void carCrashed(int playerId)
	{
		sendCommand(new Command("carCrashed").append(playerId));
	}
	
	/**
	 * Notifies the client that the specified players car finished.
	 * 
	 * @param playerId  The player ID.
	 */
	public void carFinished(int playerId)
	{
		sendCommand(new Command("carFinished").append(playerId));
	}
	
	/**
	 * Notifies the client that the game has ended.
	 * 
	 * @param highScores  The high scores to display to the player. 
	 */
	public void endGame(List<Score> highScores)
	{
		Command command = new Command("endGame");
		for (Score score : highScores)
			command.append(score.toString());
		sendCommand(command);
	}
	
	/**
	 * Notifies the client that the specified player has left.
	 * 
	 * @param playerId  The ID of the player that left.
	 */
	public void playerLeft(int playerId)
	{
		sendCommand(new Command("playerLeft").append(playerId));
	}
	
	@Override
	public void run()
	{
		try {
			while (!socket.isInputShutdown()) {
				String line = in.readLine();
				if (line == null)
					break;
				Command command = Command.parseCommand(line);
				if (command == null)
					throw new CommException("Malformed command: " + line);
				processCommand(command);
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "Exception in client handler", e);
		} finally {
			if (game != null)
				game.part(this);

			try {
				if (!socket.isClosed())
					socket.close();
			} catch (IOException e) {
				logger.log(Level.WARNING, "Unable to close client socket", e);
			}
			
			logger.info("Client disconnect (id = " + id + ").");
		}
	}

	private void sendCommand(Command command)
	{
		out.println(command.toString());
		logger.info(command.toString());
		if (out.checkError()) {
			logger.warning("Error while sending command, disconnecting.");
			disconnect();
		}
	}

	private void processCommand(Command command) throws RacetrackException
	{
		// If the client haven't said "hello" yet, that's the only command we
		// allow
		if (name == null) {
			logger.log(Level.INFO, command.toString());
			if (command.getName().equals("hello"))
				processHello(command);
			else
				throw new CommException("Unexpected command, expected hello.");
		} else {
			switch (command.getName()) {
			case "start":
				processStart(command);
				break;
				
			case "drive":
				processDrive(command);
				break;
				
			default:
				throw new CommException("Unknown command \"" + command.getName() + "\".");
			}
		}
	}

	private void processHello(Command command) throws RacetrackException
	{
		name = command.argument(0);
		logger.info("Client authenticated as \"" + name + "\", id = " + id);
		sendCommand(new Command("welcome").append(id));
		
		game = server.getGame();
		if (!game.join(this)) {
			sendCommand(new Command("busy"));
			disconnect();
		}
	}
	
	private void processStart(Command command) throws RacetrackException
	{
		int mapId = command.intArgument(0);
		boolean enableCollisions = command.booleanArgument(1);
		game.start(this, mapId, enableCollisions);
	}
	
	private void processDrive(Command command) throws RacetrackException
	{
		game.drive(this, new Vector2i(
				command.intArgument(0),
				command.intArgument(1)));
	}
	
	private void disconnect()
	{
		try {
			socket.shutdownInput();
		} catch (IOException e) {
			logger.log(Level.WARNING, "Unable to shut down socket input.", e);
		}
	}
}
