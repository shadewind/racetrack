package net.shadewind.racetrack.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.shadewind.racetrack.RaceMap;

/**
 * Main server runnable. Listens for connections and handles them.
 * 
 * @author Emil Eriksson
 */
public class Server implements Runnable {
	private static final Logger logger = Logger
			.getLogger("net.shadewind.racetrack.server.Server");

	private final HighscoreManager highscoreManager = new HighscoreManager(
			FileSystems.getDefault().getPath("highscore.txt"));
	private final Game game = new Game(loadMaps(), highscoreManager);
	

	@Override
	public void run()
	{
		highscoreManager.load();
		
		try (ServerSocket serverSocket = new ServerSocket(1337)) {
			logger.log(Level.INFO, "Server running.");
			while (true) {
				Socket socket = serverSocket.accept();
				logger.info("Client connected: " + socket.getRemoteSocketAddress());
				Client client = new Client(this, socket);
				new Thread(client, "Client thread").start();
			}
		} catch (IOException e) {
			logger.log(Level.SEVERE, "I/O error in server", e);
		}
	}
	
	private List<RaceMap> loadMaps()
	{
		List<RaceMap> maps = new ArrayList<RaceMap>();
		try {
			maps.add(new RaceMap(FileSystems.getDefault().getPath("map00.txt")));
			maps.add(new RaceMap(FileSystems.getDefault().getPath("map01.txt")));
			maps.add(new RaceMap(FileSystems.getDefault().getPath("map02.txt")));
		} catch (IOException e) {
			logger.log(Level.WARNING, "Unable to load all maps.", e);
		}
		
		return maps;
	}

	/**
	 * Returns the main <code>Game</code>.
	 * 
	 * @return The main game object.
	 */
	public Game getGame()
	{
		return game;
	}
	
	/**
	 * Returns the high score manager.
	 * 
	 * @return The high score manager.
	 */
	public HighscoreManager getHighscoreManager()
	{
		return highscoreManager;
	}
}
