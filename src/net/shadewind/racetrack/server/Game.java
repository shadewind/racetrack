package net.shadewind.racetrack.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import net.shadewind.racetrack.Car;
import net.shadewind.racetrack.GameState;
import net.shadewind.racetrack.RaceMap;
import net.shadewind.racetrack.Score;
import net.shadewind.racetrack.Vector2i;
import net.shadewind.racetrack.client.Player;

public class Game {
	private static final Logger logger = Logger
			.getLogger("net.shadewind.racetrack.server.Game");
	private static final int TIMEOUT = 5;
	
	private final Timer timer = new Timer(true);
	private final HighscoreManager highscoreManager;
	private final List<RaceMap> maps = new ArrayList<RaceMap>();
	private final List<Client> clients = new ArrayList<Client>();
	private GameState state = GameState.LOBBY;
	
	private RaceMap map;
	private boolean collisionsEnabled;
	private final Map<Client, Car> cars = new HashMap<Client, Car>();
	private final List<Client> activeClients = new ArrayList<Client>();
	private int turnPointer;
	private TimerTask timeoutTask;
	
	/**
	 * Creates a new <code>Game</code> object.
	 * 
	 * @param maps              The maps that can be chosen in the game.
	 * @param highscoreManager  The highscoreManager to use.
	 */
	public Game(List<RaceMap> maps, HighscoreManager highscoreManager)
	{
		this.maps.addAll(maps);
		this.highscoreManager = highscoreManager;
	}
	
	/**
	 * Joins the specified client to the game making it part of it. The client
	 * will be notified of all game events.
	 * 
	 * @param client  The client that wants to join.
	 * @return <code>true</code> if the client joined successfully, <code>false</code> if
	 *         the client can't join right now.
	 */
	public synchronized boolean join(Client client)
	{
		if (state != GameState.LOBBY)
			return false;
		
		clients.add(client);
		
		for (Client c : clients) {
			//Do not notify the client about itself twice!
			if (c != client)
				c.newPlayer(client.getId(), client.getName());
			//However, the client should receive notification about itself once.
			client.newPlayer(c.getId(), c.getName());
		}
		
		// Notify about available maps.
		for (int i = 0; i < maps.size(); i++)
			client.mapAvailable(i, maps.get(i).getName());
				
		//The first client is the game master.
		client.lobby(clients.get(0).getId());
		
		logger.info("Client joined (id = "  + client.getId() + ")");
		return true;
	}

	/**
	 * Removes the specified client from the game.
	 * 
	 * @param client the client to remove
	 */
	public synchronized void part(Client client)
	{
		if (state == GameState.LOBBY) {
			boolean wasGameMaster = (client == clients.get(0));
			clients.remove(client);
			cars.remove(client);
			if (wasGameMaster && !clients.isEmpty()) {
				// Assign a new game master.
				for (Client c : clients)
					c.lobby(clients.get(0).getId());
			}
		} else if (state == GameState.STARTED) {
			boolean wasTurnPlayer = (activeClients.get(turnPointer) == client);
			deactivateClient(client);
			if (activeClients.isEmpty())
				endGame();
			else if (wasTurnPlayer)
				nextTurn();			
		}
		
		for (Client c : clients)
			c.playerLeft(client.getId());
		logger.info("Client parted game (id = " + client.getId() + ").");
	}
	
	/**
	 * Starts the game.
	 * 
	 * @param client            The client that starts the game. Must be the game master.
	 * @param mapId             The ID of the map.
	 * @param enableCollisions  Whether to enable collisions or not.
	 */
	public synchronized void start(Client client, int mapId, boolean enableCollisions)
			throws GameException
	{
		if (client != clients.get(0))
			throw new GameException("Only the game master may start the game.");
		
		if (mapId >= maps.size())
			throw new GameException("Invalid map ID chosen.");
		
		map = maps.get(mapId);
		collisionsEnabled = enableCollisions;
		
		activeClients.clear();
		activeClients.addAll(clients);
		Collections.shuffle(activeClients);
				
		List<Vector2i> startPositions = map.getStartPositions();
		int position = 0;
		cars.clear();
		for (Client c : clients) {
			Car car = new Car();
			car.pushPosition(startPositions.get(position));
			cars.put(c, car);
			position = (position + 1) % startPositions.size();
		}
		
		for (Client c : clients) {
			c.gameStarted(map, collisionsEnabled);
			for (Client carClient : clients) {
				Car car = cars.get(carClient);
				c.carReset(carClient.getId(), car.getPosition());
			}
		}
		
		state = GameState.STARTED;
		turnPointer = -1;
		nextTurn();
	}
	
	/**
	 * Drives the specified clients car.
	 * 
	 * @param client  The client.
	 * @param acc     The acceleration.
	 */
	public synchronized void drive(Client client, Vector2i acc)
	{
		if (activeClients.get(turnPointer) != client)
			return;
		
		Car car = cars.get(client);
		car.drive(acc);
		logger.info("Car " + client.getId() + " drove " + acc.x() + "," + acc.y());
		
		for (Client c : clients)
			c.carDriven(client.getId(), car.getPosition(), car.getVelocity());

		checkCrash(client);
		checkFinish(client);
		nextTurn();
	}
	
	private void deactivateClient(Client client)
	{
		int index = activeClients.indexOf(client);
		if (index == -1)
			return;
		
		if (index <= turnPointer) {
			turnPointer--;
			if (turnPointer < 0)
				turnPointer = activeClients.size() - 1;
		}
		activeClients.remove(client);
	}
	
	private void checkCrash(Client client)
	{
		Car car = cars.get(client);
		Vector2i position = car.getPosition();
		if (map.getBlockType(position.x(), position.y()) == '#') {
			deactivateClient(client);
			car.setCrashed(true);
			for (Client c : clients)
				c.carCrashed(client.getId());
		}
		
		if (collisionsEnabled) {
			for (Client otherClient : clients) {
				if (client == otherClient)
					continue;
				
				Car otherCar = cars.get(otherClient);
				if (car.getPosition().equals(otherCar.getPosition())) {
					deactivateClient(client);
					car.setCrashed(true);
					for (Client c : clients)
						c.carCrashed(client.getId());
					break;
				}
			}
		}
	}

	private void checkFinish(Client client)
	{
		Car car = cars.get(client);
		List<Vector2i> path = car.getPath();
		List<Vector2i> pointPath = new ArrayList<Vector2i>();
		for (int i = 0; i < (path.size() - 1); i++)
			pointPath.addAll(drawLine(path.get(i), path.get(i + 1)));
		
		int checkpointCount = 0;
		int totalCheckpoints = map.getCheckpoints();
		for (Vector2i v : pointPath) {
			//System.out.println(v.toString());
			char block = map.getBlockType(v.x(), v.y());
			switch (block)
			{
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				int n = block - '0';
				if ((checkpointCount + 1) == n)
					checkpointCount = n;
				break;
				
			case 'E':
				if (checkpointCount == totalCheckpoints) {
					deactivateClient(client);
					car.setFinished(true);
					for (Client c : clients)
						c.carFinished(client.getId());
				}
				return;		
			}
		}
	}
	
	private static List<Vector2i> drawLine(Vector2i start, Vector2i end)
	{
		if (start.equals(end))
			return Collections.emptyList();
			
		int dx = Math.abs(start.x() - end.x());
		int dy = Math.abs(start.y() - end.y());
		if (dx > dy)
			return drawLineDx(start, end);
		else
			return drawLineDy(start, end);
	}

	private static List<Vector2i> drawLineDx(Vector2i start, Vector2i end)
	{
		List<Vector2i> line = new ArrayList<Vector2i>();
		int dx = Math.abs(start.x() - end.x());
		int stepX = (start.x() < end.x()) ? 1 : -1;
		double stepY = (double)(end.y() - start.y()) / dx;
		double y = start.y();
		for (int x = start.x(); x != end.x(); x += stepX) {
			line.add(new Vector2i(x, (int)Math.round(y)));
			y += stepY;
		}
		
		return line;
	}

	private static List<Vector2i> drawLineDy(Vector2i start, Vector2i end)
	{
		List<Vector2i> line = new ArrayList<Vector2i>();
		int dy = Math.abs(start.y() - end.y());
		int stepY = (start.y() < end.y()) ? 1 : -1;
		double stepX = (double)(end.x() - start.x()) / dy;
		double x = start.x();
		for (int y = start.y(); y != end.y(); y += stepY) {
			line.add(new Vector2i((int)Math.round(x), y));
			x += stepX;
		}
		
		return line;
	}

	private void nextTurn()
	{
		cancelTimeout();
		
		int nActive = activeClients.size();
		if (nActive == 0) {
			endGame();
			return;
		}
		
		turnPointer = (turnPointer + 1) % nActive;
		final Client client = activeClients.get(turnPointer);
		
		for (Client c : clients)
			c.playerTurn(client.getId(), TIMEOUT);
		
		//Timeout timer
		timeoutTask = new TimerTask() {
			@Override
			public void run() { drive(client, new Vector2i(0, 0)); }
		};
		timer.schedule(timeoutTask, TIMEOUT * 1000);
	}
	
	private void endGame()
	{
		cancelTimeout();
		
		List<Score> scores = new ArrayList<Score>();
		for (Client client : clients) {
			Car car = cars.get(client);
			if (car.isFinished()) {
				scores.add(new Score(
						client.getName(),
						car.getPath().size() - 1,
						map.getName(),
						collisionsEnabled));
			}
		}
		highscoreManager.addScores(scores);
		List<Score> highScores = highscoreManager.getScores(map.getName(), collisionsEnabled);
		for (Client c : clients) {
			c.endGame(highScores);
			c.lobby(clients.get(0).getId());
		}
		
		state = GameState.LOBBY;
	}
	
	private void cancelTimeout()
	{
		if (timeoutTask != null) {
			timeoutTask.cancel();
			timeoutTask = null;
		}
	}
}
