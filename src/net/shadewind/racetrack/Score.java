package net.shadewind.racetrack;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Represents a score on a map.
 * 
 * @author shadewind
 */
public class Score implements Comparable<Score> {
	private String playerName;
	private int steps;
	private String mapName;
	private boolean collisionsEnabled;
	
	/**
	 * Constructs a new <code>Score</code>.
	 * 
	 * @param playerName         The name of the player.
	 * @param steps              The number of steps.
	 * @param mapName            The map name.
	 * @param collisionsEnabled  Whether collisions were enabled.
	 */
	public Score(String playerName, int steps, String mapName,
			boolean collisionsEnabled)
	{
		super();
		this.playerName = playerName;
		this.steps = steps;
		this.mapName = mapName;
		this.collisionsEnabled = collisionsEnabled;
	}
	
	/**
	 * Returns the name of the player.
	 * 
	 * @return The player name.
	 */
	public String getPlayerName()
	{
		return playerName;
	}
	
	/**
	 * Returns the number of steps.
	 * 
	 * @return The number of steps taken.
	 */
	public int getSteps()
	{
		return steps;
	}
	
	/**
	 * Returns the name of the map.
	 * 
	 * @return The map name.
	 */
	public String getMapName()
	{
		return mapName;
	}
	
	/**
	 * Returns whether collisions were enable or not.
	 * 
	 * @return Return whether collisions were enabled.
	 */
	public boolean isCollisionsEnabled()
	{
		return collisionsEnabled;
	}
	
	/**
	 * Returns a string representation of this high score containing no
	 * space characters.
	 * 
	 * @return String representation.
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		try {
			builder.append(URLEncoder.encode(playerName, "UTF-8"));
			builder.append("|");
			builder.append(Integer.toString(steps));
			builder.append("|");
			builder.append(URLEncoder.encode(mapName, "UTF-8"));
			builder.append("|");
			builder.append(collisionsEnabled ? "1" : "0");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		}
		
		return builder.toString();
	}
	
	/**
	 * Parses a score from a string.
	 * 
	 * @param str  The stringt o parse.
	 * @return A score or <code>null</code> if it could not be parsed.
	 */
	public static Score parse(String str)
	{
		String[] split = str.split("\\|");
		if (split.length < 4)
			return null;
		
		try {
			String playerName = URLDecoder.decode(split[0], "UTF-8");
			int steps = Integer.parseInt(split[1]);
			String mapName = URLDecoder.decode(split[2], "UTF-8");
			boolean collisionEnabled = Integer.parseInt(split[3]) != 0;
			return new Score(playerName, steps, mapName, collisionEnabled);
		} catch (NumberFormatException e) {
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Compares by number of steps.
	 * 
	 * @param o  The object to compare with.
	 */
	@Override
	public int compareTo(Score o)
	{
		return steps - o.steps;
	}
}
