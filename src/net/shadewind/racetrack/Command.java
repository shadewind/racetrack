package net.shadewind.racetrack;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for simplifying the handling of commands.
 * 
 * @author Emil Eriksson
 */
public class Command {
	private final String name;
	private final List<String> arguments = new ArrayList<String>();

	/**
	 * Constructs a new command with the specified command name.
	 * 
	 * @param name  the command name
	 */
	public Command(String name)
	{
		this.name = name;
	}

	/**
	 * Returns the name of the command.
	 * 
	 * @return the command name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Returns the argument at the specified position.
	 * 
	 * @param n  the index of the argument
	 * @return the argument at the specified position
	 * @throws CommException  If the argument doesn't exist.
	 */
	public String argument(int n) throws CommException
	{
		if (n >= arguments.size())
			throw new CommException("Missing argument " + n + ", count is " + arguments.size());
		return arguments.get(n);
	}

	/**
	 * Returns the specified argument as an integer.
	 * 
	 * @param n  the index of the argument
	 * @return the specified argument as an integer
	 * @throws CommException  If the argument is not a valid integer or doesn't exist.
	 */
	public int intArgument(int n) throws CommException
	{
		String str = arguments.get(n);
		try {
			return Integer.parseInt(str);
		} catch (NumberFormatException e) {
			throw new CommException("Invalid number format: " + str, e);
		}
	}
	
	/**
	 * Returns the specified argument as a boolean. The argument will first
	 * be converted to an integer.
	 * 
	 * @param n  The index of the argument.
	 * @return The specified argument as a boolean.
	 * @throws CommException  If the argument is not a valid integer or doesn't exist. 
	 */
	public boolean booleanArgument(int n) throws CommException
	{
		return intArgument(n) != 0;
	}

	/**
	 * Returns the number of argument in this command.
	 * 
	 * @return the number of arguments
	 */
	public int argCount()
	{
		return arguments.size();
	}

	/**
	 * Appends the specified string as an argument.
	 * 
	 * @param arg the argument to append
	 * @return this commmand
	 */
	public Command append(String arg)
	{
		arguments.add(arg);
		return this;
	}

	/**
	 * Appends the specified integer as an argument.
	 * 
	 * @param arg  the argument to append
	 * @return this command
	 */
	public Command append(int arg)
	{
		return append(Integer.toString(arg));
	}

	/**
	 * Appends the specified float as an argument.
	 * 
	 * @param arg  the argument to append
	 * @return this command
	 */
	public Command append(float arg)
	{
		return append(Float.toString(arg));
	}

	/**
	 * Appends the specified double as an argument.
	 * 
	 * @param arg  The argument to append.
	 * @return This command.
	 */
	public Command append(double arg)
	{
		return append(Double.toString(arg));
	}
	
	/**
	 * Appends the specified boolean as an argument. It will be converted
	 * to an int and then appended.
	 * 
	 *  @param arg  The argument to append.
	 *  @return This command.
	 */
	public Command append(boolean arg)
	{
		return append(arg ? 1 : 0);
	}

	/**
	 * Contructs a command from the specified line string.
	 * 
	 * @param line  the line to parse
	 * @return the parsed command or <code>null</code> if the string is not a
	 *         valid command
	 */
	public static Command parseCommand(String line)
	{
		if (line == null)
			return null;

		String[] split = line.split(" ");
		if (split.length == 0)
			return null;

		Command command = new Command(split[0]);
		for (int i = 1; i < split.length; i++)
			command.append(split[i]);

		return command;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(name);
		for (String argument : arguments)
			builder.append(" ").append(argument);
		return builder.toString();
	}
}
