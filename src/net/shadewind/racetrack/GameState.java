package net.shadewind.racetrack;

/**
 * Describes the state of a game.
 * 
 * @author Emil Eriksson
 */
public enum GameState {
	/**
	 * Game is in lobby mode waiting for game master to start the game.
	 */
	LOBBY,
	
	/**
	 * Game is started.
	 */
	STARTED
}