package net.shadewind.racetrack;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a racetrack map.
 * 
 * @author Emil Eriksson
 */
public class RaceMap {
	private String name = "Map";
	private final List<String> rows = new ArrayList<String>();
	private int width = 0;
	
	/**
	 * Constructs a new <code>RaceMap</code> from the specified string
	 * The string may use CRLF, LF or pipe characters ('|') for separation of rows.
	 * 
	 * @param str  The string.
	 */
	public RaceMap(String str)
	{
		this(Arrays.asList(str.split("(\r?\n)|\\|")));
	}
	
	/**
	 * Constructs a new <code>RaceMap</code> from the specified array of strings where
	 * each string represents a row in the map.
	 *
	 * @param rawRows  The row array.
	 */
	public RaceMap(List<String> rawRows)
	{
		// Find the width.
		for (String rawRow : rawRows)
			width = (rawRow.length() > width) ? rawRow.length() : width;
			
		// Add padded versions of the strings to the rows.
		for (String rawRow : rawRows) {
			StringBuilder builder = new StringBuilder(rawRow);
			while (builder.length() < width)
				builder.append('#');
			//Replace all _ with ' '.
			String row = builder.toString().replace('_', ' ');
			rows.add(row);
		}
	}
	
	/**
	 * Loads a new <code>RaceMap</code> from a file.
	 * 
	 * @param file  The file to load from.
	 * @throws IOException  On failure to read the file. 
	 */
	public RaceMap(Path file) throws IOException
	{
		this(Files.readAllLines(file, StandardCharsets.US_ASCII));
		name = file.getFileName().toString();
	}
	
	/**
	 * Returns the block type at the specified position.
	 *
	 * @param x  The X coordinate.
	 * @param y  The Y coordinate.
	 * 
	 * @return The block type as a char.
	 */
	public char getBlockType(int x, int y)
	{
		return rows.get(rows.size() - y - 1).charAt(x);
	}
	
	/**
	 * Returns the width of the map.
	 * 
	 * @return The width.
	 */
	public int getWidth()
	{
		return width;
	}
	
	/**
	 * Returns the height of the map.
	 * 
	 * @return The height.
	 */
	public int getHeight()
	{
		return rows.size();
	}
	
	/**
	 * Returns the name of the map.
	 * 
	 * @return The name of the map.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Sets the name of the map.
	 * 
	 * @param name  The name to set.
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	/**
	 * Returns all of the start positions available in the map.
	 * 
	 * @return A <code>List</code> of the start positions.
	 */
	public List<Vector2i> getStartPositions()
	{
		List<Vector2i> startPositions = new ArrayList<Vector2i>();
		for (int y = 0; y < getHeight(); y++) {
			for (int x = 0; x < getWidth(); x++) {
				if (getBlockType(x, y) == 'S')
					startPositions.add(new Vector2i(x, y));
			}
		}
		
		return startPositions;
	}
	
	/**
	 * Returns the total number of checkpoints in the map.
	 * 
	 * @return The total number of checkpoints.
	 */
	public int getCheckpoints()
	{
		int maxCheckpoint = 0;
		for (int y = 0; y < getHeight(); y++) {
			for (int x = 0; x < getWidth(); x++) {
				char block = getBlockType(x, y);
				if (Character.isDigit(block))
					maxCheckpoint = Math.max(maxCheckpoint, block - '0');
			}
		}
		
		return maxCheckpoint;
	}
	
	/**
	 * Returns a string representation of this map with rows separated by pipe chars.
	 * All spaces are replaced with underscores to simplify sending over network.
	 * 
	 * @return A string representation.
	 */
	public String toNetworkString()
	{
		StringBuilder builder = new StringBuilder();
		for (String row : rows) {
			builder.append(row);
			builder.append('|');
		}
		
		return builder.substring(0, builder.length() - 1).replace(' ', '_');
	}
}
