package net.shadewind.racetrack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents a car in the race.
 * 
 * @author Emil Eriksson
 */
public class Car {
	private final List<Vector2i> path = new ArrayList<Vector2i>();
	private Vector2i velocity = new Vector2i(0, 0);
	private boolean crashed = false;
	private boolean finished = false;

	/**
	 * Pushes the specified vector on to the path list.
	 * 
	 * @param v The new position vector.
	 */
	public void pushPosition(Vector2i v)
	{
		path.add(v);
	}

	/**
	 * Adjusts the velocity according to <code>acc</code> and pushes a new
	 * position onto the path according to the new velocity.
	 */
	public void drive(Vector2i acc)
	{
		int x = Math.max(Math.min(acc.x(), 1), -1);
		int y = Math.max(Math.min(acc.y(), 1), -1);
		velocity = velocity.sum(new Vector2i(x, y));
		pushPosition(getPosition().sum(velocity));
	}

	/**
	 * Returns the path.
	 * 
	 * @return The path.
	 */
	public List<Vector2i> getPath()
	{
		return new ArrayList<Vector2i>(path);
	}

	/**
	 * Returns the velocity.
	 * 
	 * @return The velocity.
	 */
	public Vector2i getVelocity()
	{
		return velocity;
	}

	/**
	 * Sets the velocity.
	 * 
	 * @param velocity The velocity to set.
	 */
	public void setVelocity(Vector2i velocity)
	{
		this.velocity = velocity;
	}

	/**
	 * Returns the current position. This is the same as taking the end of the
	 * path. Returns <code>null</code> if the path is empty.
	 * 
	 * @return The position.
	 */
	public Vector2i getPosition()
	{
		if (path.isEmpty())
			return null;

		return path.get(path.size() - 1);
	}

	/**
	 * Returns true if the car is crashed.
	 * 
	 * @return <code>true</code> if the car is crashed.
	 */
	public boolean isCrashed()
	{
		return crashed;
	}

	/**
	 * Sets the crash status.
	 * 
	 * @param crashed The crash status to set.
	 */
	public void setCrashed(boolean crashed)
	{
		this.crashed = crashed;
	}

	/**
	 * Returns the finish status of the car.
	 * 
	 * @return The finish status.
	 */
	public boolean isFinished()
	{
		return finished;
	}
	
	/**
	 * Sets the finish status.
	 * 
	 * @param finished  The status to set.
	 */
	public void setFinished(boolean finished)
	{
		this.finished = finished;
	}
	
	/**
	 * Resets this car.
	 */
	public void reset()
	{
		path.clear();
		velocity = new Vector2i(0, 0);
		finished = false;
		crashed = false;
	}
}
