package net.shadewind.racetrack;

/**
 * Thrown to indicate a unexpected response from the other party in network
 * communication.
 * 
 * @author Emil Eriksson
 */
public class CommException extends RacetrackException {
	private static final long serialVersionUID = 1L;

	public CommException()
	{
		super();
	}

	public CommException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public CommException(String arg0)
	{
		super(arg0);
	}

	public CommException(Throwable arg0)
	{
		super(arg0);
	}

}
