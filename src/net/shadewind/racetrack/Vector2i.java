package net.shadewind.racetrack;

import java.awt.geom.Point2D;
import java.io.Serializable;

/**
 * Two-dimensional vector class which can be used to represent positions and
 * directions. This class is immutable.
 * 
 * @author Emil Eriksson
 */
public class Vector2i implements Serializable {
	private final int x;
	private final int y;

	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new Vector2d with the specified X and Y components.
	 * 
	 * @param x
	 *            the X component
	 * @param y
	 *            the Y component
	 */
	public Vector2i(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns the X component.
	 * 
	 * @return the X component.
	 */
	public int x()
	{
		return x;
	}

	/**
	 * Returns the Y component.
	 * 
	 * @return the Y component
	 */
	public int y()
	{
		return y;
	}

	/**
	 * Returns the sum of this and the specified vector.
	 * 
	 * @param rhs
	 *            the other vector
	 * @return the sum
	 */
	public Vector2i sum(Vector2i rhs)
	{
		return new Vector2i(x + rhs.x, y + rhs.y);
	}

	/**
	 * Returns the difference between this vector and the specified vector (i.e.
	 * </code>this - rhs</code>.
	 * 
	 * @param rhs
	 *            the right hand side vector
	 * @return the difference
	 */
	public Vector2i sub(Vector2i rhs)
	{
		return new Vector2i(x - rhs.x, y - rhs.y);
	}

	/**
	 * Returns this vector multiplied by the specified scalar.
	 * 
	 * @param scalar
	 *            the scalar
	 * @return this vector multiplied by the scalar
	 */
	public Vector2i mul(int scalar)
	{
		return new Vector2i(x * scalar, y * scalar);
	}

	/**
	 * Returns the dot product of this vector and the specicied one.
	 * 
	 * @param rhs
	 *            the other vector
	 * @return the dot product
	 */
	public int dot(Vector2i rhs)
	{
		return x * rhs.x + y * rhs.y;
	}

	/**
	 * Returns the angle of this vector.
	 * 
	 * @return the angle in radians
	 */
	public double angle()
	{
		return Math.atan2(x, y);
	}

	/**
	 * Returns the norm of this vector.
	 * 
	 * @return the norm
	 */
	public double norm()
	{
		return Math.sqrt(x * x + y * y);
	}

	/**
	 * Converts this vector to a <code>Point2D</code>.
	 * 
	 * @return a Point2D equal to this vector
	 */
	public Point2D toPoint2D()
	{
		return new Point2D.Double(x, y);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof Vector2i))
			return false;

		Vector2i other = (Vector2i) obj;
		return x == other.x && y == other.y;
	}

	@Override
	public int hashCode()
	{
		return x * 257 + y * 13;
	}

	@Override
	public String toString()
	{
		return "(" + x + ", " + y + ")";
	}
}
