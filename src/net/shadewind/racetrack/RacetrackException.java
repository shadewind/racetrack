package net.shadewind.racetrack;

/**
 * Common superclass for the custom exceptions in this game.
 * 
 * @author Emil Eriksson
 */
public class RacetrackException extends Exception {
	private static final long serialVersionUID = 1L;

	public RacetrackException()
	{
		super();
	}

	public RacetrackException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public RacetrackException(String arg0)
	{
		super(arg0);
	}

	public RacetrackException(Throwable arg0)
	{
		super(arg0);
	}

}
