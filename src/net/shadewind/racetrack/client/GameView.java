package net.shadewind.racetrack.client;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.Timer;

import net.shadewind.racetrack.Car;
import net.shadewind.racetrack.RaceMap;
import net.shadewind.racetrack.Score;
import net.shadewind.racetrack.Vector2i;

/**
 * Main game view.
 * 
 * @author Emil Eriksson
 */
public class GameView extends JComponent {
	private static final long serialVersionUID = 1L;
	
	private static final double TILE_SIZE = 12.0;
	private static final Color COLOR_WALL = new Color(0.2f, 0.2f, 0.2f);
	private static final Color COLOR_START = new Color(1.0f, 0.5f, 0.5f);
	private static final Color COLOR_END = new Color(0.5f, 1.0f, 0.5f);
	private static final Color COLOR_CHECKPOINT = new Color(0.5f, 0.5f, 0.5f);
	private static final Color COLOR_GRID = new Color(0.8f, 0.8f, 0.8f);
	private static final Color[] COLORS = new Color[] {
		Color.BLUE, Color.RED, Color.GREEN, Color.CYAN,
		Color.MAGENTA, Color.YELLOW, Color.ORANGE
	};
	private static final Font MESSAGE_FONT = new Font("Arial", Font.PLAIN, 64);
	
	private RacetrackClient client;
	private boolean showAcceleratorControls = false;
	private int acceleratorX = 0;
	private int acceleratorY = 0;
	private String message;
	private long timerEnd = -1;
	private Timer timer = new Timer(1000, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) { repaint(); }
	});
	
	/**
	 * Creates a new <code>GameView</code>.
	 */
	public GameView()
	{
		addKeyListener(new AcceleratorKeyListener());
		setFocusable(true);
	}
	
	/**
	 * Sets the client whose state to draw.
	 * 
	 * @param client 
	 */
	public void setClient(RacetrackClient client)
	{
		this.client = client;
	}
	
	/**
	 * Sets whether to show accelerator controls or not.
	 * 
	 * @param enable  Whether to show the controls.
	 */
	public void setShowAcceleratorControls(boolean enable)
	{
		showAcceleratorControls = enable;
		repaint();
	}
	
	/**
	 * Returns the current chosen acceleration.
	 * 
	 * @return The acceleration.
	 */
	public Vector2i getAcceleration()
	{
		return new Vector2i(acceleratorX, acceleratorY);
	}
	
	/**
	 * Displays a message.
	 * 
	 * @param message  The message to display.
	 */
	public void displayMessage(String message)
	{
		this.message = message;
		repaint();
	}
	
	/**
	 * Sets the timer to count down from the specified number of seconds. Set to 0 to
	 * disable.
	 *
	 * @param seconds  The number of seconds.
	 */
	public void setTimer(int seconds)
	{
		timer.stop();
		timer.restart();
		if (seconds == 0) {
			timerEnd = -1;
		} else {
			timerEnd = System.currentTimeMillis() + (seconds * 1000);
			timer.setDelay(1000);
			timer.setInitialDelay(1000);
			timer.setRepeats(true);
			timer.start();
		}
	}
	
	@Override
	public void paint(Graphics graphics)
	{
		Graphics2D g = (Graphics2D)graphics;
		AffineTransform identity = g.getTransform();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		g.setBackground(Color.WHITE);
		g.clearRect(0, 0, getWidth(), getHeight());
		
		//Transform so that origin is in the center of the bottom left tile and the Y-axis extends
		//upwards.
		AffineTransform transform = AffineTransform.getScaleInstance(TILE_SIZE, -TILE_SIZE);
		transform.preConcatenate(AffineTransform.getTranslateInstance(0.0, getHeight()));
		g.transform(transform);
		
		if (client == null)
			return;
		
		synchronized (client) {
			RaceMap map = client.getMap();
			if (map != null) {
				paintMap(map, g);
				paintCars(g);
				if (showAcceleratorControls)
					paintAcceleratorControls(g);
			}
		}
		
		g.setTransform(identity);
		if (message != null)
			paintMessage(message, g);
		
		if (timerEnd != -1)
			paintTimer(g);
	}
	
	private void paintTimer(Graphics2D g)
	{
		int secondsLeft = (int)(timerEnd - System.currentTimeMillis()) / 1000; 
		TextLayout layout = new TextLayout(Integer.toString(secondsLeft), MESSAGE_FONT, g.getFontRenderContext());
		Shape shape = layout.getOutline(AffineTransform.getTranslateInstance(
				getWidth() - 40.0, 60.0));
		g.setColor(Color.WHITE);
		g.fill(shape);
		g.setColor(Color.RED);
		g.setStroke(new BasicStroke(1.5f));
		g.draw(shape);
	}
	
	private void paintMessage(String str, Graphics2D g)
	{
		TextLayout layout = new TextLayout(str, MESSAGE_FONT, g.getFontRenderContext());
		float centerX = getWidth() / 2;
		float centerY = getHeight() / 2;
		Rectangle2D bounds = layout.getBounds();
		float posX = (float)(centerX - (bounds.getWidth() / 2));
		float posY = (float)(centerY - (bounds.getHeight() / 2));
		Shape shape = layout.getOutline(AffineTransform.getTranslateInstance(posX, posY));
		g.setColor(Color.WHITE);
		g.fill(shape);
		g.setColor(Color.BLACK);
		g.setStroke(new BasicStroke(1.5f));
		g.draw(shape);
	}

	private void paintAcceleratorControls(Graphics2D g)
	{
		Player player = client.getPlayer();
		Car car = player.getCar();
		Vector2i pos = car.getPosition();
		if (pos == null)
			return;
		
		double originX = pos.x() + 0.5;
		double originY = pos.y() + 0.5;
		for (int y = -1; y <= 1; y++) {
			for (int x = -1; x <= 1; x++) {
				g.setColor(Color.WHITE);
				fillCircle(originX + x, originY + y, 0.5, g);
				g.setColor(Color.BLACK);
				drawCircle(originX + x, originY + y, 0.5, g);
				if ((x == acceleratorX) && (y == acceleratorY))
					fillCircle(originX + x, originY + y, 0.3, g);
			}
		}
	}

	private void paintCars(Graphics2D g)
	{
		List<Player> players = new ArrayList<Player>(client.getPlayers());
		Collections.sort(players);
		int colorIndex = 0;
		for (Player player : players) {
			Car car = player.getCar();
			List<Vector2i> path = car.getPath();
			
			g.setColor(COLORS[colorIndex]);
			g.setStroke(new BasicStroke(0.1f));
			
			for (int i = 0; i < path.size(); i++) {
				Vector2i start = path.get(i);
				if (i < (path.size() - 1)) {
					Vector2i end = path.get(i + 1);
					Line2D line = new Line2D.Double(
							start.x() + 0.5, start.y() + 0.5,
							end.x() + 0.5, end.y() + 0.5);
					g.draw(line);
					fillCircle(start.x() + 0.5, start.y() + 0.5, 0.2, g);
				} else {
					fillCircle(start.x() + 0.5, start.y() + 0.5, 0.5, g);
				}
			}
			
			colorIndex = (colorIndex + 1) % COLORS.length;
		}
	}
	
	private void drawCircle(double x, double y, double radius, Graphics2D g)
	{
		Ellipse2D ellipse = new Ellipse2D.Double(
				x - radius, y - radius, radius * 2, radius * 2);
		g.draw(ellipse);
	}
	
	private void fillCircle(double x, double y, double radius, Graphics2D g)
	{
		Ellipse2D ellipse = new Ellipse2D.Double(
				x - radius, y - radius, radius * 2, radius * 2);
		g.fill(ellipse);
	}

	private void paintMap(RaceMap map, Graphics2D g)
	{
		int width = map.getWidth();
		int height = map.getHeight();
		
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				switch (map.getBlockType(x, y)) {
				case '#':
					g.setColor(COLOR_WALL);
					g.fillRect(x, y, 1, 1);
					break;
					
				case 'S':
					g.setColor(COLOR_START);
					g.fillRect(x, y, 1, 1);
					break;
					
				case 'E':
					g.setColor(COLOR_END);
					g.fillRect(x, y, 1, 1);
					break;
					
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					g.setColor(COLOR_CHECKPOINT);
					g.fillRect(x, y, 1, 1);
					break;
				}
			}
		}
		
		//Draw grid
		g.setStroke(new BasicStroke(0.1f));
		g.setColor(COLOR_GRID);
		for (int x = 0; x < width + 1; x++)
			g.drawLine(x, 0, x, getHeight());
		for (int y = 0; y < height + 1; y++)
			g.drawLine(0, y, getWidth(), y);
	}
	
	private class AcceleratorKeyListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e)
		{
			switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				acceleratorX--;
				if (acceleratorX < -1)
					acceleratorX = -1;
				break;
				
			case KeyEvent.VK_RIGHT:
				acceleratorX++;
				if (acceleratorX > 1)
					acceleratorX = 1;
				break;
				
			case KeyEvent.VK_UP:
				acceleratorY++;
				if (acceleratorY > 1)
					acceleratorY = 1;
				break;
				
			case KeyEvent.VK_DOWN:
				acceleratorY--;
				if (acceleratorY < -1)
					acceleratorY = -1;
				break;
			}
			message = null;
			repaint();
		}
	}
}
