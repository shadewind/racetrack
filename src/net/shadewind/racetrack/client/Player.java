package net.shadewind.racetrack.client;

import net.shadewind.racetrack.Car;

/**
 * Represents a player which is known to the client.
 * 
 * @author Emil Eriksson
 */
public class Player implements Comparable<Player> {
	int id = 0;
	private String name;
	private Car car = new Car();
	
	/**
	 * Returns the ID of the player.
	 * 
	 * @return The ID.
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * Sets the ID of the player.
	 * 
	 * @param id  The ID.
	 */
	public void setId(int id)
	{
		this.id = id;
	}
	
	/**
	 * Returns the name of the player.
	 * 
	 * @return The player name.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Sets the name of the player.
	 * 
	 * @param name  The name to set.
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	/**
	 * Returns the players car.
	 * 
	 * @return Ther car.
	 */
	public Car getCar()
	{
		return car;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	@Override
	public int compareTo(Player o)
	{
		return id - o.id;
	}
}
