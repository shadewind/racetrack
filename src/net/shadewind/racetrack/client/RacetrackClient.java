package net.shadewind.racetrack.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.shadewind.racetrack.Car;
import net.shadewind.racetrack.CommException;
import net.shadewind.racetrack.Command;
import net.shadewind.racetrack.GameState;
import net.shadewind.racetrack.RaceMap;
import net.shadewind.racetrack.Score;
import net.shadewind.racetrack.Vector2i;

/**
 * Main clientside communications class.
 * 
 * NOTE: Since there is an internal thread, the state may change at any time.
 * However, this thread always synchronizes on this object so external users of this
 * class may also do so to ensure mutual exclusion.
 * 
 * @author Emil Eriksson
 */
public class RacetrackClient {
	private static final Logger logger = Logger
			.getLogger("net.shadewind.racetrack.client.RacetrackClient");

	private String name;

	private final RacetrackClientListener listener;
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	private Thread thread;

	private int id = 0;
	private final Map<Integer, Player> players = new HashMap<Integer, Player>();
	private final Map<Integer, String> availableMaps = new HashMap<Integer, String>();
	private int gameMasterId = 0;
	private RaceMap map;
	private boolean collisionsEnabled;
	private GameState state;
	private Player turnPlayer; 
	private int turnTimeout;

	/**
	 * Constructs a new <code>TankWarsClient</code>.
	 * 
	 * @param listener The listener to notify.
	 */
	public RacetrackClient(RacetrackClientListener listener)
	{
		this.listener = listener;
		setName(null);
	}

	/**
	 * Sets the client name which is the name sent to the server. If the name is
	 * <code>null</code> the name is instead set to the system name.
	 * 
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		if (name == null)
			this.name = System.getProperty("system.name");
		else
			this.name = name;
	}

	/**
	 * Returns the client name.
	 * 
	 * @return the client name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Returns the ID assigned to the client or 0 if none has been assigned yet.
	 * 
	 * @return The ID.
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * Returns the ID of the game master or 0 if none is known yet.
	 * 
	 * @return The game master ID.
	 */
	public int getGameMasterId()
	{
		return gameMasterId;
	}
	
	/**
	 * Returns the current race map.
	 * 
	 * @return The current map.
	 */
	public RaceMap getMap()
	{
		return map;
	}
	
	/**
	 * Returns the player with the specified ID.
	 * 
	 * @param playerId  The player ID.
	 * @return The player.
	 */
	public Player getPlayer(int playerId)
	{
		synchronized (players) {
			return players.get(playerId);
		}
	}
	
	/**
	 * Returns the player object of this client or null if it is unknown.
	 * 
	 * @return The player.
	 */
	public Player getPlayer()
	{
		synchronized (players) {
			if (id == 0)
				return null;
			return players.get(id);
		}
	}
	
	/**
	 * Returns the player whose turn it is or null if it's no players turn.
	 * 
	 * @return The player whose turn it is.
	 */
	public Player getTurnPlayer()
	{
		return turnPlayer;
	}
	
	/**
	 * Returns the timeout of the current turn. The return value of this method is undefined
	 * if there is no current turn.
	 * 
	 * @return The turn timeout in seconds.
	 */
	public int getTurnTimeout()
	{
		return turnTimeout;
	}

	/**
	 * Connects this client to a server.
	 * 
	 * @param socketAddress  The address of the server.
	 * 
	 * @throws IOException on I/O errors
	 */
	public synchronized void connect(SocketAddress socketAddress) throws IOException
	{
		socket = new Socket();
		socket.connect(socketAddress);
		out = new PrintWriter(socket.getOutputStream());
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		thread = new Thread(new ReaderRunnable(), "ClientThread");
		thread.start();

		sendCommand(new Command("hello").append(name));
	}
	
	/**
	 * Tells the server to start the game. The client must be the game master.
	 * 
	 * @param map               The ID of the map. Must be one of the available maps.
	 * @param enableCollisions  Whether to enable collisions or not.
	 */
	public synchronized void startGame(int map, boolean enableCollisions)
	{
		if ((id != gameMasterId) || (id == 0))
			throw new IllegalStateException("Client is not game master.");
		
		if (!availableMaps.containsKey(map))
			throw new IllegalArgumentException("The specified map ID " + map + " is not available.");
		
		sendCommand(new Command("start").append(map).append(enableCollisions));
	}
	
	/**
	 * Returns a collection of the current players.
	 * 
	 * @return The player set.
	 */
	public Collection<Player> getPlayers()
	{
		synchronized (players) {
			return new ArrayList<Player>(players.values());
		}
	}
	
	/**
	 * Returns a <code>Map</code> of the currently available maps.
	 * 
	 * @return The maps.
	 */
	public Map<Integer, String> getAvailableMaps()
	{
		synchronized (availableMaps) {
			return new HashMap<Integer, String>(availableMaps);
		}
	}
	
	/**
	 * Notifies the server of the players intended acceleration for this turn.
	 * The server will ignore the command if it is not the players turn to avoid
	 * race conditions.
	 * 
	 * @param acc  The acceleration. Will be clamped to [-1,1] in both axes by the server.
	 */
	public void drive(Vector2i acc)
	{
		Command command = new Command("drive");
		command.append(acc.x());
		command.append(acc.y());
		sendCommand(command);
	}

	private void sendCommand(Command command)
	{
		out.println(command.toString());
		out.flush();
	}

	private void readLoop()
	{
		try {
			while (!socket.isClosed()) {
				String line = in.readLine();
				logger.info(line);
				Command command = Command.parseCommand(line);
				if (command == null)
					throw new CommException("Malformed command.");
				processCommand(command);
			}
		} catch (Exception e) {
			// TODO notify about error
			logger.log(Level.SEVERE, "Communication error.", e);
			fail();
		}
	}

	private void processCommand(Command command) throws CommException
	{
		if (id == 0) {
			switch (command.getName()) {
			case "welcome":
				processWelcome(command);
				break;
				
			default:
				throw new CommException("Unexpected command, expected welcome or busy.");
			}
		} else {
			switch (command.getName()) {
			case "player":
				processPlayer(command);
				break;
				
			case "availableMap":
				processAvailableMap(command);
				break;
				
			case "lobby":
				processLobby(command);
				break;
				
			case "gameStarted":
				processGameStarted(command);
				break;
			
			case "carReset":
				processCarReset(command);
				break;
				
			case "playerTurn":
				processPlayerTurn(command);
				break;
				
			case "carDriven":
				processCarDriven(command);
				break;
				
			case "carCrashed":
				processCarCrashed(command);
				break;
				
			case "carFinished":
				processCarFinished(command);
				break;
				
			case "endGame":
				processEndGame(command);
				break;
				
			case "playerLeft":
				processPlayerLeft(command);
				break;
				
			case "busy":
				processBusy(command);
				break;
			
			default:
				throw new CommException("Unexpected command: " + command.getName());
			}
		}
	}

	private void processWelcome(Command command) throws CommException
	{
		id = command.intArgument(0);
		logger.info("Welcomed with id = " + id);
		listener.joined();
	}
	
	private void processBusy(Command command)
	{
		try {
			socket.close();
		} catch (IOException e) {
			logger.log(Level.WARNING, "Unable to close socket.", e);
		}
		listener.serverBusy();
	}
	
	private void processPlayer(Command command) throws CommException
	{
		Player player = new Player();
		player.setId(command.intArgument(0));
		player.setName(command.argument(1));
		synchronized (players) {
			players.put(player.getId(), player);
			listener.newPlayer(player);
		}
	}
	
	private void processAvailableMap(Command command) throws CommException
	{
		int mapId = command.intArgument(0);
		String name = command.argument(1);
		synchronized (availableMaps) {
			availableMaps.put(mapId, name);
			listener.availableMap(mapId, name);
		}
	}
	
	private void processLobby(Command command) throws CommException
	{
		gameMasterId = command.intArgument(0);
		state = GameState.LOBBY;
		listener.lobby(gameMasterId);
	}
	
	private void processGameStarted(Command command) throws CommException
	{
		map = new RaceMap(command.argument(0));
		collisionsEnabled = command.booleanArgument(1);
		state = GameState.STARTED;
		listener.gameStarted(map, collisionsEnabled);
	}
	
	private void processCarReset(Command command) throws CommException
	{
		int playerId = command.intArgument(0);
		Player player = players.get(playerId);
		if (player == null)
			throw new CommException("Unknown player id = " + playerId);
		Car car = player.getCar();
		car.reset();
		car.pushPosition(new Vector2i(
				command.intArgument(1),
				command.intArgument(2)));
		listener.carReset(player);
	}
	
	private void processPlayerTurn(Command command) throws CommException
	{
		turnPlayer = players.get(command.intArgument(0));
		turnTimeout = command.intArgument(1);
		listener.playerTurn(turnPlayer, turnTimeout);
	}
	
	private void processCarDriven(Command command) throws CommException
	{
		Player player = players.get(command.intArgument(0));
		if (player == null)
			throw new CommException("Player is unknown.");
		
		Car car = player.getCar();
		car.pushPosition(new Vector2i(command.intArgument(1), command.intArgument(2)));
		car.setVelocity(new Vector2i(command.intArgument(3), command.intArgument(4)));
		listener.carDriven(player);
	}
	
	private void processCarCrashed(Command command) throws CommException
	{
		Player player = players.get(command.intArgument(0));
		if (player == null)
			throw new CommException("Player is unknown.");
		
		Car car = player.getCar();
		car.setCrashed(true);
		listener.carCrashed(player);
	}
	
	private void processCarFinished(Command command) throws CommException
	{
		Player player = players.get(command.intArgument(0));
		if (player == null)
			throw new CommException("Player is unknown.");
		
		Car car = player.getCar();
		car.setFinished(true);
		listener.carFinished(player);
	}
	
	private void processEndGame(Command command) throws CommException
	{
		List<Score> highScores = new ArrayList<Score>();
		for (int i = 0; i < command.argCount(); i++) {
			Score score = Score.parse(command.argument(i));
			if (score != null)
				highScores.add(score);
		}
		
		listener.endGame(highScores);
	}
	
	private void processPlayerLeft(Command command) throws CommException
	{
		Player player = players.get(command.intArgument(0));
		if (player == null)
			throw new CommException("Player is unknown.");
		
		synchronized (players) {
			players.remove(player.getId());
			listener.playerLeft(player);
		}
	}

	private void fail()
	{
		listener.fail();
	}

	public class ReaderRunnable implements Runnable {
		@Override
		public void run()
		{
			readLoop();
		}
	}
}
