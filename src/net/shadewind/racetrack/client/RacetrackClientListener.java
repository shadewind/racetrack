package net.shadewind.racetrack.client;

import java.util.List;

import net.shadewind.racetrack.RaceMap;
import net.shadewind.racetrack.Score;

/**
 * Users of the <code>RacetrackClient</code> should implement this interface to
 * receive notifications about events.
 * 
 * All methods are called from the client thread so synchronization measures must be taken.
 * 
 * @author Emil Eriksson
 */
public interface RacetrackClientListener {
	/**
	 * Called when the client has joined the game.
	 */
	void joined();
	
	/**
	 * Called to indicate that the game is in lobby mode.
	 * 
	 * @param id  The ID of the game master.
	 */
	void lobby(int id);
	
	/**
	 * Called when the client is notified of a new player.
	 * 
	 * @param player  The new player.
	 */
	void newPlayer(Player player);
	
	/**
	 * Called when the client is notified of a new available map.
	 * 
	 * @param id    The map ID.
	 * @param name  The map name
	 */
	void availableMap(int id, String name);
	
	/**
	 * Called to indicate that the game has started.
	 * 
	 * @param map                The map used to play.
	 * @param collisionsEnabled  Whether collisions are enabled or not.
	 */
	void gameStarted(RaceMap map, boolean collisionsEnabled);
	
	/**
	 * Called when the position of a player car is reset and the path cleared.
	 * 
	 * @param player  The player for which the position was cleared.
	 */
	void carReset(Player player);
	
	/**
	 * Called when the player turn changes.
	 * 
	 * @param player   The player whose turn it is.
	 * @param timeout  The timeout of the turn.
	 */
	void playerTurn(Player player, int timeout);
	
	/**
	 * Called when a car has been driven.
	 * 
	 * @param player  The player whose car was driven.
	 */
	void carDriven(Player player);
	
	/**
	 * Called when a car has crashed.
	 * 
	 * @param player  The player whose car crashed.
	 */
	void carCrashed(Player player);
	
	/**
	 * Called when a car has finished.
	 * 
	 * @param player  The player whose car finished.
	 */
	void carFinished(Player player);
	
	/**
	 * Called when the game ends.
	 * 
	 * @param highScores  The high scores.
	 */
	void endGame(List<Score> highScores);
	
	/**
	 * Called when a players leaves the game.
	 * 
	 * @param player  The player that left.
	 */
	void playerLeft(Player player);
	
	/**
	 * Called to indicate that the server was busy and the game
	 * could not be joined.
	 */
	void serverBusy();
	
	/**
	 * Called when there is a client failure.
	 */
	void fail();
}
