package net.shadewind.racetrack.client;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import net.shadewind.racetrack.RaceMap;
import net.shadewind.racetrack.Score;

/**
 * Main game window.
 * 
 * @author Emil Eriksson
 */
public class MainWindow extends JFrame implements RacetrackClientListener {
	private static final long serialVersionUID = 1L;

	private final GameView gameView;
	
	private RacetrackClient client;
	private LobbyDialog lobbyDialog;

	/**
	 * Constructs a new <code>MainWindow</code>.
	 */
	public MainWindow()
	{
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		gameView = new GameView();
		
		Container contentPane = getContentPane();
		GroupLayout layout = new GroupLayout(contentPane);
		contentPane.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(gameView, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addComponent(gameView, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		
		Dimension size = new Dimension(800, 800);
		setMinimumSize(size);
		setMaximumSize(size);
		
		gameView.addKeyListener(new MainKeyListener());
	}

	/**
	 * If not connected already, shows the connect dialog and connects.
	 */
	public void connect()
	{
		if (client != null)
			return;

		try {
			client = new RacetrackClient(this);
			ConnectDialog connectDialog = new ConnectDialog(this);
			connectDialog.setModal(true);
			connectDialog.setVisible(true);
			if (connectDialog.isAccepted()) {
				client.setName(connectDialog.getName());
				client.connect(connectDialog.getSocketAddress());
			} else {
				System.exit(0);
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this,
					"Unable to connect: " + e.getMessage(), "Connection error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void joined()
	{
		//Nothing right now
	}

	@Override
	public void lobby(final int id)
	{
		onEdtBlocking(new Runnable() {
			@Override
			public void run()
			{
				if (lobbyDialog == null) {
					lobbyDialog = new LobbyDialog(client, MainWindow.this);
					lobbyDialog.setVisible(true);
				}
				lobbyDialog.setEnableGameMaster(client.getId() == id);
			}
		});
	}

	@Override
	public void newPlayer(final Player player)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				if (lobbyDialog != null)
					lobbyDialog.addPlayer(player);
			}
		});
	}

	@Override
	public void availableMap(final int id, final String name)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				if (lobbyDialog != null)
					lobbyDialog.addMap(id, name);
			}
		});
	}
	
	@Override
	public void gameStarted(RaceMap map, boolean collisionsEnabled)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				lobbyDialog.setVisible(false);
				lobbyDialog = null;
				gameView.setClient(client);
				gameView.displayMessage(null);
				gameView.repaint();
				pack();
			}
		});
	}
	
	@Override
	public void carReset(Player player)
	{
		repaintGameView();
	}
	
	@Override
	public void playerTurn(final Player player, final int timeout)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				gameView.setShowAcceleratorControls(player == client.getPlayer());
				gameView.setTimer(timeout);
			}
		});
	}
	
	@Override
	public void carDriven(Player player)
	{
		repaintGameView();
	}
	
	@Override
	public void carCrashed(final Player player)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				String who = player.getName();
				if (player == client.getPlayer())
					who = "You";
				
				gameView.displayMessage(who + " crashed!");
			}
		});
	}
	
	@Override
	public void carFinished(final Player player)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				String who = player.getName();
				if (player == client.getPlayer())
					who = "You";
				
				gameView.displayMessage(who + " finished!");
			}
		});
	}
	
	@Override
	public void endGame(final List<Score> highScores)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				gameView.displayMessage("Game ended!");
				gameView.setTimer(0);
				new HighScoreWindow(highScores).setVisible(true);
			}
		});
	}
	
	@Override
	public void playerLeft(final Player player)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				gameView.displayMessage(player.getName() + " left the game");
				if (lobbyDialog != null)
					lobbyDialog.removePlayer(player);
			}
		});
	}
	
	@Override
	public void serverBusy()
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				client = null;
				JOptionPane.showMessageDialog(MainWindow.this,
						"Server is currently busy with an ongoing game. " + 
						"Wait for it to finish or try another server.");
				connect();
			}
		});
	}
	
	@Override
	public void fail()
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				JOptionPane.showMessageDialog(
						MainWindow.this,
						"A client error ocurred. Game will now exit.", "Client error",
						JOptionPane.ERROR_MESSAGE);
			}
		});
	}
	
	//Repaints the game view on the EDT.
	private void repaintGameView()
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				gameView.repaint();
			}
		});
	}

	// Convenience method so we don't have to catch
	// stupid exceptions that are never ever thrown.
	private void onEdtBlocking(Runnable runnable)
	{
		try {
			EventQueue.invokeAndWait(runnable);
		} catch (Exception e) {
			// InterruptedException can only happen if someone calls interrupt()
			// on the EDT... and why would you do that...?
			// InvocationTargetException should only be thrown if our runnable
			// throws an exception and why would it?
			e.printStackTrace();
		}
	}
	
	private class MainKeyListener extends KeyAdapter
	{
		@Override
		public void keyPressed(KeyEvent e)
		{
			if (e.getKeyCode() == KeyEvent.VK_SPACE) {
				if (client.getPlayer() == client.getTurnPlayer())
					client.drive(gameView.getAcceleration());
			}
		}
	}
}
