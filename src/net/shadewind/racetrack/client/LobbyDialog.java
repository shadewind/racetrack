package net.shadewind.racetrack.client;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 * The lobby window. Shows the user list and lets the game master select
 * level.
 * 
 * @author Emil Eriksson
 */
public class LobbyDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private final RacetrackClient client;
	private final JList<Player> playerList;
	private final JLabel mapsLabel;
	private final JComboBox<String> mapsCombobox;
	private final JCheckBox collisionCheckbox;
	private final JButton startButton;
	
	private final List<Player> players = new ArrayList<Player>();
	private final List<Integer> mapIds = new ArrayList<Integer>();
	
	/**
	 * Creates a new <code>LobbyWindow</code> with the speicified owner.
	 * 
	 * @param client  The client.
	 * @param owner   The owner.
	 */
	public LobbyDialog(RacetrackClient client, Frame owner)
	{
		super(owner);
		this.client = client;
		
		setTitle("Lobby");
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		JLabel playerLabel = new JLabel("Players:");
		playerList = new JList<Player>();
		mapsLabel = new JLabel("Maps:");
		mapsCombobox = new JComboBox<String>();
		collisionCheckbox = new JCheckBox("Enable collisions");
		startButton = new JButton("Start");
		startButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				LobbyDialog.this.client.startGame(
						mapIds.get(mapsCombobox.getSelectedIndex()),
						collisionCheckbox.isSelected());
			}
		});
		setEnableGameMaster(this.client.getId() == this.client.getGameMasterId());
		
		Container contentPane = getContentPane();
		GroupLayout layout = new GroupLayout(contentPane);
		contentPane.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(playerLabel)
				.addComponent(playerList, 
						0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(mapsLabel)
				.addComponent(mapsCombobox,
						GroupLayout.PREFERRED_SIZE,
						GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addComponent(collisionCheckbox)
				.addComponent(startButton));
		
		layout.setHorizontalGroup(layout.createParallelGroup()
				.addComponent(playerLabel)
				.addComponent(playerList,
						0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(mapsLabel)
				.addComponent(mapsCombobox)
				.addComponent(collisionCheckbox)
				.addComponent(startButton));
		
		setSize(300, 600);
		
		for (Player player : client.getPlayers())
			addPlayer(player);
		
		Map<Integer, String> maps = client.getAvailableMaps();
		for (int id : maps.keySet())
			addMap(id, maps.get(id));
	}
	
	/**
	 * Enables or disables game master controls.
	 * 
	 * @param enable  Whether to enable or disable. 
	 */
	public void setEnableGameMaster(boolean enable)
	{
		mapsLabel.setEnabled(enable);
		mapsCombobox.setEnabled(enable);
		collisionCheckbox.setEnabled(enable);
		startButton.setEnabled(enable);
		
		if (enable)
			setTitle("You are the game master.");
		else
			setTitle("Waiting for game master to start game.");
	}
	
	/**
	 * Adds a new player to the player list.
	 * 
	 * @param player  The player to add.
	 */
	public void addPlayer(Player player)
	{
		players.add(player);
		playerList.setListData(players.toArray(new Player[players.size()]));
	}
	
	/**
	 * Removes the specified player from the player list.
	 * 
	 * @param player  The player to remove.
	 */
	public void removePlayer(Player player)
	{
		players.remove(player);
		playerList.setListData(players.toArray(new Player[players.size()]));
	}
	
	/**
	 * Adds a new map to the map list.
	 * 
	 * @param id    The map ID.
	 * @param name  The map name. 
	 */
	public void addMap(int id, String name)
	{
		mapIds.add(id);
		mapsCombobox.addItem(name);
	}
}
