package net.shadewind.racetrack.client;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import net.shadewind.racetrack.Score;

/**
 * Window for displaying high scores.
 * 
 * @author Emil Eriksson
 */
public class HighScoreWindow extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private final JTable table;
	private final DefaultTableModel tableModel = new DefaultTableModel();
		
	/**
	 * Constructs a new \c JFrame.
	 * 
	 * @param highScores  The high scores to display.
	 */
	public HighScoreWindow(List<Score> highScores)
	{
		setTitle("High score");
		tableModel.addColumn("Steps");
		tableModel.addColumn("Player name");
		for (Score score : highScores)
			tableModel.addRow(new String[] { Integer.toString(score.getSteps()), score.getPlayerName() } );
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);
		JButton closeButton = new JButton("Close");
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				setVisible(false);
			}
		});
		
		Container contentPane = getContentPane();
		GroupLayout layout = new GroupLayout(contentPane);
		contentPane.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(scrollPane,
						0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(closeButton));
		
		layout.setHorizontalGroup(layout.createParallelGroup()
				.addComponent(scrollPane,
						0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(closeButton));
		
		setSize(400, 600);
	}
}
