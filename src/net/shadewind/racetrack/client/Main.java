package net.shadewind.racetrack.client;

/**
 * Main entry point for client application.
 */
public class Main {
	/**
	 * Standard main function.
	 */
	public static void main(String[] args) throws Exception
	{
		MainWindow mainWindow = new MainWindow();
		mainWindow.setVisible(true);
		mainWindow.connect();
	}
}
