package net.shadewind.racetrack.client;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetSocketAddress;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * Shows a dialog in which the user can enter data to connect to the server.
 * 
 * @author Emil Eriksson
 */
public class ConnectDialog extends JDialog {
	private static final long serialVersionUID = 1L;

	private JTextField nameField;
	private JTextField hostField;
	private boolean accepted = false;;

	/**
	 * Creates an unowned <code>ConnectDialog</code>.
	 */
	public ConnectDialog()
	{
		setup();
	}

	/**
	 * Creates a new <code>ConnectDialog</code>.
	 * 
	 * @param owner  The owner of the dialog.
	 */
	public ConnectDialog(Frame owner)
	{
		super(owner);
		setup();
	}

	/**
	 * Returns the socket address.
	 * 
	 * @return The socket address.
	 */
	public InetSocketAddress getSocketAddress()
	{
		return new InetSocketAddress(hostField.getText(), 1337);
	}

	/**
	 * Returns the name.
	 * 
	 * @return The name.
	 */
	public String getName()
	{
		return nameField.getText();
	}

	/**
	 * Returns <code>true</code> if the user clicked "Connect".
	 * 
	 * @return <code>true</code> if the user clicked "Connect",
	 *         </code>false</code> on "Cancel"
	 */
	public boolean isAccepted()
	{
		return accepted;
	}

	private void setup()
	{
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		setTitle("Connect...");
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;

		JLabel nameLabel = new JLabel("Name:");
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(nameLabel, gbc);
		nameField = new JTextField();
		gbc.gridx = 1;
		gbc.gridy = 0;
		add(nameField, gbc);

		JLabel hostLabel = new JLabel("Host:");
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(hostLabel, gbc);
		hostField = new JTextField();
		gbc.gridx = 1;
		gbc.gridy = 1;
		add(hostField, gbc);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				accepted = false;
				setVisible(false);

			}
		});
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(cancelButton, gbc);
		
		JButton connectButton = new JButton("Connect");
		connectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				accepted = true;
				setVisible(false);

			}
		});
		gbc.gridx = 1;
		gbc.gridy = 2;
		add(connectButton, gbc);

		pack();
	}
}